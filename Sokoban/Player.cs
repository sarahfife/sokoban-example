﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace Sokoban
{
    class Player : Tile
    {


        // ------------------
        // Behaviour
        // ------------------
        public Player(Texture2D newTexture)
            : base(newTexture)
        {
        }
        // ------------------
        public override void Update(GameTime gameTime)
        {
            float frameTime = (float)gameTime.ElapsedGameTime.TotalSeconds;

            // Get the current keyboard state
            KeyboardState keyboardState = Keyboard.GetState();

            // Check specific keys and record movement
            Vector2 movementInput = Vector2.Zero;


            if (keyboardState.IsKeyDown(Keys.A))
            {
                movementInput.X = -1.0f;
                TryMove(movementInput);
            }
            else if (keyboardState.IsKeyDown(Keys.D))
            {
                movementInput.X = 1.0f;
                TryMove(movementInput);
            }

            if (keyboardState.IsKeyDown(Keys.W))
            {
                movementInput.Y = -1.0f;
                TryMove(movementInput);
            }
            else if (keyboardState.IsKeyDown(Keys.S))
            {
                movementInput.Y = 1.0f;
                TryMove(movementInput);
            }
        }
        // ------------------
        public bool TryMove(Vector2 direction)
        {
            Vector2 newGridPos = GetTilePosition() + direction;

            

            return false;
        }
        // ------------------
    }
}
