﻿using Microsoft.Xna.Framework.Graphics;

namespace Sokoban
{
    class Wall : Tile
    {

        // ------------------
        // Behaviour
        // ------------------
        public Wall(Texture2D newTexture)
            : base(newTexture)
        {
        }
        // ------------------
    }
}
